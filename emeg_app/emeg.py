from apis import apur_transmissao
from apis import carga
from apis import dessem
from apis import energia_agora
from apis import hidrologia
from apis import pdp
from apis import proc_op_rede
from apis import sgi_op
from apis import teleassistencia

def main():
    # Chamadas para os métodos das APIs
    apur_transmissao.apuracao()
    carga.carga()
    dessem.dessem()
    energia_agora.carga()
    hidrologia.hidrologia()
    pdp.api()
    proc_op_rede.rede()
    sgi_op.intervencao()
    teleassistencia.teleassistencia()

if __name__ == "__main__":
    main()